package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {
    private static final String INSERT_USER = "INSERT INTO users (login) VALUES (?)";
    private static final String FIND_ALL_USERS = "SELECT * FROM users";
    private static final String INSERT_TEAM = "INSERT INTO teams (name) VALUES (?)";
    private static final String FIND_ALL_TEAMS = "SELECT * FROM teams";
    private static final String SET_TEAMS_FOR_USER = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
    private static final String GET_USER = "SELECT * FROM users WHERE login=(?)";
    private static final String GET_TEAM = "SELECT * FROM teams WHERE name=(?)";
    private static final String GET_USER_TEAMS = "SELECT id, name FROM teams INNER JOIN users_teams ON users_teams.team_id=teams.id AND users_teams.user_id=?";
    private static final String DELETE_TEAM_CASCADE = "DELETE FROM teams WHERE teams.id=?";
    private static final String UPDATE_TEAM = "UPDATE teams SET teams.name=? WHERE teams.id=?";
    private static final String DELETE_USER_CASCADE = "DELETE FROM users WHERE users.id=?";
    private static final String APP_PROPERTIES = "app.properties";
    private static final String CONNECTION_URL = "connection.url";
    private static final String CANNOT_CLOSE_EXCEPTION = "Cannot close connection to database";
    private static final String CANNOT_SET_CONNECTION_EXCEPTION = "Cannot set connection to database. Path to database is ";
    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private DBManager() {}

    private Connection openConnection() throws DBException {
        Connection connection = null;
        String URL = null;
        try {
            InputStream inputStream = new FileInputStream(APP_PROPERTIES);
            Properties properties = new Properties();
            properties.load(inputStream);
            URL = properties.getProperty(CONNECTION_URL);
            connection = DriverManager.getConnection(URL);
            if (connection == null) {
                int indexUserData = URL.indexOf("?");
                URL = URL.substring(0, indexUserData);
                throw new DBException(CANNOT_SET_CONNECTION_EXCEPTION + URL, new NullPointerException());
            }
        } catch (SQLException ex) {
            int indexUserData = URL.indexOf("?");
            URL = URL.substring(0, indexUserData);
            throw new DBException(CANNOT_SET_CONNECTION_EXCEPTION + URL, ex);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private void closeConnection(Connection con) throws DBException {
        try {
            con.close();
        } catch (SQLException throwablese) {
            throw new DBException(CANNOT_CLOSE_EXCEPTION, throwablese);
        }
    }

    private void checkForNull(Connection con) throws DBException {
        if (con == null) {
            throw new DBException("Instance of Connection class is null", new NullPointerException());
        }
    }

    public List<User> findAllUsers() throws DBException {
        Connection con = openConnection();
        checkForNull(con);
        List<User> users = new ArrayList<>();
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(FIND_ALL_USERS);
            while (rs.next()) {
                User user = User.createUser(rs.getString(2));
                user.setId(rs.getInt(1));
                users.add(user);
            }
        } catch (SQLException e) {
            throw new DBException("Cannot find users in database", e);
        } finally {
            closeConnection(con);
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        if (user == null || user.getLogin() == null) {
            throw new DBException("User login cannot be null", new NullPointerException());
        }
        Connection con = openConnection();
        checkForNull(con);
        try {
            PreparedStatement ps = con.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, user.getLogin());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                user.setId(rs.getInt(1));
                return true;
            }
            return false;
        } catch (SQLException e) {
            throw new DBException("Cannot insert user to database", e);
        } finally {
            closeConnection(con);
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        if (users == null) {
            throw new DBException("The array of users are null", new IllegalArgumentException());
        }
        Connection con = openConnection();
        checkForNull(con);
        try {
            PreparedStatement ps = con.prepareStatement(DELETE_USER_CASCADE);
            con.setAutoCommit(false);
            for (User user : users) {
                if (user == null || user.getLogin() == null) {
                    throw new DBException("User or login cannot be null", new NullPointerException());
                }
                ps.setInt(1, user.getId());
                ps.executeUpdate();
            }
            con.commit();
        } catch (SQLException e) {
            try {
                con.rollback();
            } catch (SQLException throwables) {
                throw new DBException("Cannot rollback changes in database", throwables);
            }
            throw new DBException("Cannot delete users from database", e);
        } finally {
            try {
                con.setAutoCommit(true);
                con.close();
            } catch (SQLException throwablese) {
                throw new DBException(CANNOT_CLOSE_EXCEPTION, throwablese);
            }
        }
        return false;
    }

    public User getUser(String login) throws DBException {
        if (login == null) {
            throw new DBException("User login cannot be null", new NullPointerException());
        }
        Connection con = openConnection();
        checkForNull(con);
        try {
            PreparedStatement ps = con.prepareStatement(GET_USER);
            ps.setString(1, login);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                User user = User.createUser(rs.getString(2));
                user.setId(rs.getInt(1));
                return user;
            }
        } catch (SQLException e) {
            throw new DBException("Cannot get user from database", e);
        } finally {
            closeConnection(con);
        }
        return null;
    }

    public Team getTeam(String name) throws DBException {
        if (name == null) {
            throw new DBException("Team name cannot be null", new NullPointerException());
        }
        Connection con = openConnection();
        checkForNull(con);
        try {
            PreparedStatement ps = con.prepareStatement(GET_TEAM);
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Team team = Team.createTeam(rs.getString(2));
                team.setId(rs.getInt(1));
                return team;
            }
        } catch (SQLException e) {
            throw new DBException("Cannot get team from database", e);
        } finally {
            closeConnection(con);
        }
        return null;
    }

    public List<Team> findAllTeams() throws DBException {
        Connection con = openConnection();
        checkForNull(con);
        List<Team> teams = new ArrayList<>();
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(FIND_ALL_TEAMS);
            while (rs.next()) {
                Team team = Team.createTeam(rs.getString(2));
                team.setId(rs.getInt(1));
                teams.add(team);
            }
        } catch (SQLException e) {
            throw new DBException("Cannot find teams in database", e);
        } finally {
            closeConnection(con);
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        if (team == null || team.getName() == null) {
            throw new DBException("Team cannot be null", new NullPointerException());
        }
        Connection con = openConnection();
        checkForNull(con);
        try {
            PreparedStatement ps = con.prepareStatement(INSERT_TEAM, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, team.getName());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                team.setId(rs.getInt(1));
                return true;
            }
            return false;
        } catch (SQLException e) {
            throw new DBException("Cannot insert team", e);
        } finally {
            closeConnection(con);
        }
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        if (teams == null) {
            throw new DBException("The array of teams cannot be null", new NullPointerException());
        }
        Connection con = openConnection();
        checkForNull(con);
        try {
            con.setAutoCommit(false);
            PreparedStatement ps = con.prepareStatement(SET_TEAMS_FOR_USER, Statement.RETURN_GENERATED_KEYS);
            for (Team team : teams) {
                ps.setInt(1, user.getId());
                ps.setInt(2, team.getId());
                ps.executeUpdate();
            }
            con.commit();
            return true;
        } catch (Exception e) {
            try {
                con.rollback();
            } catch (SQLException throwables) {
                throw new DBException("Cannot rollback changes in database", throwables);
            }
            throw new DBException("Cannot set teams for user", e);
        } finally {
            try {
                con.setAutoCommit(true);
            } catch (SQLException throwablese) {
                throw new DBException("Cannot set auto commit to database", throwablese);
            }
            closeConnection(con);
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        Connection con = openConnection();
        checkForNull(con);
        if (user == null) {
            throw new DBException("Obtained user is null", new NullPointerException());
        }
        List<Team> teams = new ArrayList<>();
        try {
            PreparedStatement ps = con.prepareStatement(GET_USER_TEAMS);
            ps.setInt(1, user.getId());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Team team = Team.createTeam(rs.getString(2));
                team.setId(rs.getInt(1));
                teams.add(team);
            }
        } catch (SQLException e) {
            throw new DBException("Cannot get user from database", e);
        } finally {
            closeConnection(con);
        }
        return teams;
    }

    public boolean deleteTeam(Team team) throws DBException {
        if (team == null || team.getName() == null) {
            throw new DBException("Obtained team is null", new NullPointerException());
        }
        Connection con = openConnection();
        checkForNull(con);
        try {
            PreparedStatement ps = con.prepareStatement(DELETE_TEAM_CASCADE);
            ps.setInt(1, team.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Cannot delete team from database", e);
        } finally {
            closeConnection(con);
        }
        return false;
    }

    public boolean updateTeam(Team team) throws DBException {
        if (team == null || team.getName() == null) {
            throw new DBException("Obtained team is null", new NullPointerException());
        }
        Connection con = openConnection();
        checkForNull(con);
        try {
            PreparedStatement ps = con.prepareStatement(UPDATE_TEAM);
            ps.setString(1, team.getName());
            ps.setInt(2, team.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Cannot update team in database", e);
        } finally {
            closeConnection(con);
        }
        return false;
    }
}
